`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:12:51 04/15/2015 
// Design Name: 
// Module Name:    rippley_carry_adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rippley_carry_adder(
    input [3:0] a,
    input [3:0] b,
    input cin,
    output reg [3:0] sum
    );

always @*
begin
	sum = a + b + cin;
end

endmodule

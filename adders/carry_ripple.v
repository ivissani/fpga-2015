`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:15:49 04/15/2015 
// Design Name: 
// Module Name:    carry_ripple 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module carry_ripple #(parameter WIDTH=4)
(
    input [WIDTH-1:0] a,
    input [WIDTH-1:0] b,
    output reg [WIDTH:0] sum
);

reg [WIDTH:0] carry;
integer ii;

always @*
begin
	carry[0] = 1'b0;
	for(ii = 0; ii < WIDTH; ii = ii + 1)
	begin
		carry[ii + 1] = (a[ii] & b[ii]) | ((a[ii] ^ b[ii]) & carry[ii]);
		sum[ii] = a[ii] ^ b[ii] ^ carry[ii];
	end
	sum[WIDTH] = carry[WIDTH];
end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:31:57 04/15/2015 
// Design Name: 
// Module Name:    carry_la_adder_4b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module carry_la_adder_4b(
	 input [3:0] a,
    input [3:0] b,
    input cin,
    output [3:0] s,
    output cout
    );

wire [3:0] p;
wire [3:0] g;
wire [3:0] carry;

carry_la_adder_1b adder0(.a(a[0]),.b(b[0]),.cin(cin),.g(g[0]),.p(p[0]),.s(s[0]));
carry_la_adder_1b adder1(.a(a[1]),.b(b[1]),.cin(carry[0]),.g(g[1]),.p(p[1]),.s(s[1]));
carry_la_adder_1b adder2(.a(a[2]),.b(b[2]),.cin(carry[1]),.g(g[2]),.p(p[2]),.s(s[2]));
carry_la_adder_1b adder3(.a(a[3]),.b(b[3]),.cin(carry[2]),.g(g[3]),.p(p[3]),.s(s[3]));

carry_la_predictor_4b predictor(.cin(cin), .carry(carry), .p(p), .g(g));

assign cout = carry[3];

endmodule

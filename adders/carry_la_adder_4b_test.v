`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:55:53 04/15/2015
// Design Name:   carry_la_adder_4b
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/adders/carry_la_adder_4b_test.v
// Project Name:  adders
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: carry_la_adder_4b
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module carry_la_adder_4b_test;

	// Inputs
	reg [3:0] a;
	reg [3:0] b;
	reg cin;

	// Outputs
	wire [3:0] s;
	wire cout;

	// Instantiate the Unit Under Test (UUT)
	carry_la_adder_4b uut (
		.a(a), 
		.b(b), 
		.cin(cin), 
		.s(s), 
		.cout(cout)
	);
	
	integer i;
	integer j;

	initial begin
		for(i = 0; i < 16; i = i + 1)
			for(j = 0; j < 16; j = j + 1)
				begin
					// Initialize Inputs
					a = i;
					b = j;
					cin = 0;

					// Wait 100 ns for global reset to finish
					#100;
				end
		$stop;
	end
      
endmodule


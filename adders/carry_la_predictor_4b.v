`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:39:19 04/15/2015 
// Design Name: 
// Module Name:    carry_la_predictor_4b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module carry_la_predictor_4b(
    input cin,
    input [3:0] p,
    input [3:0] g,
    output [3:0] carry
    );

assign carry[0] = g[0] | (cin & p[0]);
assign carry[1] = g[1] | (g[0] & p[1]) | (cin & p[0] & p[1]);
assign carry[2] = g[2] | (p[2] & g[1]) | (p[2] & p[1] & g[0]) | (p[2] & p[1] & p[0] & cin);
assign carry[3] = g[3] | (p[3] & g[2]) | (p[3] & p[2] & g[1]) | (p[3] & p[2] & p[1] & g[0]) | (p[3] & p[2] & p[1] & p[0] & cin);

endmodule

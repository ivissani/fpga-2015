`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:23:05 04/15/2015 
// Design Name: 
// Module Name:    carry_la_adder_1b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module carry_la_adder_1b(
    input a,
    input b,
    input cin,
    output s,
    output p,
    output g
    );

assign p = a ^ b;
assign g = a & b;
assign s = p ^ cin;

endmodule

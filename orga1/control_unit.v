`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:22:01 05/20/2015 
// Design Name: 
// Module Name:    control_unit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module control_unit(
	input clk,
	input reset,
	
	output reg [4:0] bi_mutex_sel,
	
	output reg alu_op_add,
	output reg alu_op_sub,
	output reg alu_op_and,
	output reg alu_op_or,
	output reg alu_op_not,

	output reg alu_enable_in1,
	output reg alu_enable_in2,
	
	output reg mem_enable_address_in,
	output reg mem_enable_data_in,
	output reg mem_op_read,
	output reg mem_op_write,
	
	output reg signext_enable_in,
	
	output reg regs_enable_pc_write,
	output reg regs_enable_ir0_write,
	output reg regs_enable_ir1_write,
	output reg regs_enable_ir2_write,
	output reg regs_enable_sp_write,
	output reg regs_enable_r0_write,
	output reg regs_enable_r1_write,
	output reg regs_enable_r2_write,
	output reg regs_enable_r3_write,
	output reg regs_enable_r4_write,
	output reg regs_enable_r5_write,
	output reg regs_enable_r6_write,
	output reg regs_enable_r7_write,
	output reg regs_enable_op_src_write,
	output reg regs_enable_op_dst_write,
	output reg regs_enable_z_write,
	output reg regs_enable_c_write,
	output reg regs_enable_n_write,
	output reg regs_enable_v_write,
	
	input mem_ready,
	input z,
	input c,
	input v,
	input n,
	input [15:0] ir0
    );

localparam STATE_INITIAL = 0;
// First fetch states
localparam STATE_FETCH = 1;
localparam STATE_FETCH_2 = 2;
localparam STATE_FETCH_PC_INC_0 = 3;
localparam STATE_FETCH_PC_INC_1 = 4;
localparam STATE_FETCH_PC_INC_2 = 5;
// Second fetch states
localparam STATE_FETCH_FIRST_OPERAND = 6;
localparam STATE_FETCH_FIRST_OPERAND_2 = 7;
localparam STATE_FETCH_FIRST_OPERAND_PC_INC_0 = 8;
localparam STATE_FETCH_FIRST_OPERAND_PC_INC_1 = 9;
localparam STATE_FETCH_FIRST_OPERAND_PC_INC_2 = 10;
// Third fetch states
localparam STATE_FETCH_SECOND_OPERAND = 11;
localparam STATE_FETCH_SECOND_OPERAND_2 = 12;
localparam STATE_FETCH_SECOND_OPERAND_PC_INC_0 = 13;
localparam STATE_FETCH_SECOND_OPERAND_PC_INC_1 = 14;
localparam STATE_FETCH_SECOND_OPERAND_PC_INC_2 = 15;

// Execute states depending on mode
localparam STATE_EXECUTE_FETCH_SRC = 16;
localparam STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE = 17;
localparam STATE_EXECUTE_FETCH_SRC_INDIRECT = 18;
localparam STATE_EXECUTE_FETCH_SRC_INDEXED_0 = 19;
localparam STATE_EXECUTE_FETCH_SRC_INDEXED_1 = 20;

localparam STATE_EXECUTE_FETCH_DST = 21;
localparam STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE = 22;
localparam STATE_EXECUTE_FETCH_DST_INDIRECT = 23;
localparam STATE_EXECUTE_FETCH_DST_INDEXED_0 = 24;
localparam STATE_EXECUTE_FETCH_DST_INDEXED_1 = 25;

localparam STATE_EXECUTE_EXECUTE = 26;

localparam STATE_EXECUTE_ALU_OP_0 = 27;
localparam STATE_EXECUTE_ALU_STORE = 28;
localparam STATE_EXECUTE_ALU_ADDC = 29;
localparam STATE_EXECUTE_ALU_ADDC_2 = 31;

localparam STATE_EXECUTE_STORE_DIRECT = 31;
localparam STATE_EXECUTE_STORE_DIRECT_2 = 32;
localparam STATE_EXECUTE_STORE_INDIRECT = 33;
localparam STATE_EXECUTE_STORE_INDIRECT_2 = 34;
localparam STATE_EXECUTE_STORE_REG_DIRECT = 35;
localparam STATE_EXECUTE_STORE_REG_INDEXED = 36;
localparam STATE_EXECUTE_STORE_REG_INDEXED_2 = 37;
localparam STATE_EXECUTE_STORE_REG_INDEXED_3 = 38;

localparam STATE_EXECUTE_JXX_1 = 39;
localparam STATE_EXECUTE_JXX_2 = 40;
localparam STATE_EXECUTE_JXX_3 = 41;

localparam STATE_EXECUTE_STORE_C = 42;
localparam STATE_EXECUTE_STORE_V = 43;
localparam STATE_EXECUTE_STORE_N = 44;
localparam STATE_EXECUTE_STORE_Z = 45;

localparam STATE_EXECUTE_CALL_1 = 46;
localparam STATE_EXECUTE_CALL_2 = 47;
localparam STATE_EXECUTE_CALL_3 = 48;
localparam STATE_EXECUTE_CALL_4 = 49;
localparam STATE_EXECUTE_CALL_5 = 50;

localparam STATE_EXECUTE_RET_1 = 51;
localparam STATE_EXECUTE_RET_2 = 52;
localparam STATE_EXECUTE_RET_3 = 53;
localparam STATE_EXECUTE_RET_4 = 54;

localparam STATE_WAIT_OP_FINISH = 32'hfffffffe;
localparam STATE_INVALID_OP = 32'hffffffff;

reg [31:0] state_reg = STATE_INITIAL;
reg [31:0] state_next = STATE_FETCH;
reg [31:0] state_saved = STATE_INVALID_OP;
reg [31:0] state_saved_reg = STATE_INVALID_OP;

//reg [26:0] counter_reg = 26'b0;

always@(posedge clk, posedge reset)
begin
	if(reset)
		state_reg <= STATE_INITIAL;
	else
		if(mem_ready)
		begin
			state_reg <= state_next;
			state_saved_reg <= state_saved;
		end
		else
		begin	
			state_reg <= state_reg;
			state_saved_reg <= state_saved_reg;
		end
end

reg invalid_op_reg = 1'b0;

wire [3:0] opcode = ir0[15:12];
wire [3:0] opcode2 = ir0[11:8];
wire [5:0] dst = ir0[11:6];
wire [5:0] src = ir0[5:0];

// FSM control unit	
always@*
begin
	state_saved = state_saved_reg;

	bi_mutex_sel = 0;
	
	alu_op_add = 0;
	alu_op_sub = 0;
	alu_op_and = 0;
	alu_op_or = 0;
	alu_op_not = 0;

	alu_enable_in1 = 0; 
	alu_enable_in2 = 0; 

	mem_enable_address_in = 0;
	mem_enable_data_in = 0;
	mem_op_read = 0;
	mem_op_write = 0; 

	signext_enable_in = 0;

	regs_enable_pc_write = 0;
	regs_enable_ir0_write = 0;
	regs_enable_ir1_write = 0;
	regs_enable_ir2_write = 0;
	regs_enable_sp_write = 0;
	regs_enable_r0_write = 0;
	regs_enable_r1_write = 0;
	regs_enable_r2_write = 0;
	regs_enable_r3_write = 0;
	regs_enable_r4_write = 0;
	regs_enable_r5_write = 0;
	regs_enable_r6_write = 0;
	regs_enable_r7_write = 0;
	regs_enable_z_write = 0;
	regs_enable_c_write = 0;
	regs_enable_n_write = 0;
	regs_enable_v_write = 0;
	
	regs_enable_op_dst_write = 0;
	regs_enable_op_src_write = 0;
	
	state_next = STATE_INVALID_OP;
	
	case(state_reg)
	STATE_INITIAL:
	begin
		// Load 0 into PC
		regs_enable_pc_write = 1;
		bi_mutex_sel = 0;
		
		// Next we can begin to fetch instructions
		state_next = STATE_FETCH;
	end
	STATE_FETCH:
	begin
		// Load PC into MEM_ADDRESS
		mem_enable_address_in = 1;
		bi_mutex_sel = 2;
		
		// Request a read operation
		mem_op_read = 1;
		
		state_saved = STATE_FETCH_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_2:
	begin
		// Move MEM_DATA to IR0
		regs_enable_ir0_write = 1;
		bi_mutex_sel = 8;
		
		state_next = STATE_FETCH_PC_INC_0;
	end
	STATE_FETCH_PC_INC_0:
	begin
		// Load PC into ALU_IN
		alu_enable_in1 = 1;
		bi_mutex_sel = 2;
		
		state_next = STATE_FETCH_PC_INC_1;
	end
	STATE_FETCH_PC_INC_1:
	begin
		// Load 1 into ALU_IN_2
		alu_enable_in2 = 1;
		bi_mutex_sel = 1;
		
		// Request add operation
		alu_op_add = 1;
		
		state_saved = STATE_FETCH_PC_INC_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_PC_INC_2:
	begin
		// Load ALU_OUT into PC
		regs_enable_pc_write = 1;
		bi_mutex_sel = 3;
		
		if(invalid_op_reg)
			state_next = STATE_INVALID_OP;
		else
		begin
			casex(dst)
			// If mode is: Immediate, direct, indirect or indexed THEN fetch a c16 from memory INTO IR1
			6'b000000,6'b001000,6'b011000,6'b111???:
				case(opcode)
				4'b1100,4'b1111: // RET,JXX
					state_next = STATE_EXECUTE_EXECUTE;
				4'b1010,4'b1011: // JMP,CALL
					casex(src)
					// If mode is: Immediate, direct, indirect or indexed THEN fetch a c16 from memory INTO IR2
					6'b000000,6'b001000,6'b011000,6'b111???:
						state_next = STATE_FETCH_SECOND_OPERAND;
					default:
						state_next = STATE_EXECUTE_FETCH_SRC;
					endcase
				default:
					state_next = STATE_FETCH_FIRST_OPERAND;
				endcase
			default:
			begin
				casex(src)
				// If mode is: Immediate, direct, indirect or indexed THEN fetch a c16 from memory INTO IR2
				6'b000000,6'b001000,6'b011000,6'b111???:
					case(opcode)
					4'b1100,4'b1111: // RET,JXX
						state_next = STATE_EXECUTE_EXECUTE;
					default:
						state_next = STATE_FETCH_SECOND_OPERAND;
					endcase
				default:
					case(opcode)
					4'b0001,4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110,4'b1010,4'b1011: // MOV,ADD,SUB,AND,OR,ADDC,CMP,JMP,CALL
					begin
						state_next = STATE_EXECUTE_FETCH_SRC;
					end
					4'b1000,4'b1001: // NEG,NOT
					begin
						state_next = STATE_EXECUTE_FETCH_DST;
					end
					4'b1100,4'b1111: // RET,JXX
					begin
						state_next = STATE_EXECUTE_EXECUTE;
					end
					endcase
				endcase
			end
			endcase
		end
	end
	STATE_FETCH_FIRST_OPERAND:
	begin
		// Load PC into MEM_ADDRESS
		mem_enable_address_in = 1;
		bi_mutex_sel = 2;
		
		// Request a read operation
		mem_op_read = 1;
		
		state_saved = STATE_FETCH_FIRST_OPERAND_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_FIRST_OPERAND_2:
	begin
		// Move MEM_DATA to IR1
		regs_enable_ir1_write = 1;
		bi_mutex_sel = 8;
		
		state_next = STATE_FETCH_FIRST_OPERAND_PC_INC_0;
	end
	STATE_FETCH_FIRST_OPERAND_PC_INC_0:
	begin
		// Load PC into ALU_IN
		alu_enable_in1 = 1;
		bi_mutex_sel = 2;
		
		state_next = STATE_FETCH_FIRST_OPERAND_PC_INC_1;
	end
	STATE_FETCH_FIRST_OPERAND_PC_INC_1:
	begin
		// Load 1 into ALU_IN_2
		alu_enable_in2 = 1;
		bi_mutex_sel = 1;
		
		// Request add operation
		alu_op_add = 1;
		
		state_saved = STATE_FETCH_FIRST_OPERAND_PC_INC_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_FIRST_OPERAND_PC_INC_2:
	begin
		// Load ALU_OUT into PC
		regs_enable_pc_write = 1;
		bi_mutex_sel = 3;
		
		
		casex(src)
		6'b000000,6'b001000,6'b011000,6'b111???:
			state_next = STATE_FETCH_SECOND_OPERAND;
		default:
			case(opcode)
			4'b0001,4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110,4'b1010,4'b1011: // MOV,ADD,SUB,AND,OR,ADDC,CMP,JMP,CALL
			begin
				state_next = STATE_EXECUTE_FETCH_SRC;
			end
			4'b1000,4'b1001: // NEG,NOT
			begin
				state_next = STATE_EXECUTE_FETCH_DST;
			end
			4'b1100,4'b1111: // RET,JXX
			begin
				state_next = STATE_EXECUTE_EXECUTE;
			end
			endcase
		endcase
	end
	STATE_FETCH_SECOND_OPERAND:
	begin
		// Load PC into MEM_ADDRESS
		mem_enable_address_in = 1;
		bi_mutex_sel = 2;
		
		// Request a read operation
		mem_op_read = 1;
		
		state_saved = STATE_FETCH_SECOND_OPERAND_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_SECOND_OPERAND_2:
	begin
		// Move MEM_DATA to IR2
		regs_enable_ir2_write = 1;
		bi_mutex_sel = 8;
		
		state_next = STATE_FETCH_SECOND_OPERAND_PC_INC_0;
	end
	STATE_FETCH_SECOND_OPERAND_PC_INC_0:
	begin
		// Load PC into ALU_IN
		alu_enable_in1 = 1;
		bi_mutex_sel = 2;
		
		state_next = STATE_FETCH_SECOND_OPERAND_PC_INC_1;
	end
	STATE_FETCH_SECOND_OPERAND_PC_INC_1:
	begin
		// Load 1 into ALU_IN_2
		alu_enable_in2 = 1;
		bi_mutex_sel = 1;
		
		// Request add operation
		alu_op_add = 1;
		
		state_saved = STATE_FETCH_SECOND_OPERAND_PC_INC_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_FETCH_SECOND_OPERAND_PC_INC_2:
	begin
		// Load ALU_OUT into PC
		regs_enable_pc_write = 1;
		bi_mutex_sel = 3;
		
		case(opcode)
		4'b0001,4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110,4'b1010,4'b1011: // MOV,ADD,SUB,AND,OR,ADDC,CMP,JMP,CALL
		begin
			state_next = STATE_EXECUTE_FETCH_SRC;
		end
		4'b1000,4'b1001: // NEG,NOT
		begin
			state_next = STATE_EXECUTE_FETCH_DST;
		end
		4'b1100,4'b1111: // RET,JXX
		begin
			state_next = STATE_EXECUTE_EXECUTE;
		end
		endcase
	end
	STATE_EXECUTE_FETCH_SRC:
	begin
		casex(src)
		6'b001000: // direct mode [c16]
		begin
			bi_mutex_sel = 22; // IR2
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b011000: // indirect mode [[c16]]
		begin
			bi_mutex_sel = 22; // IR2
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_SRC_INDIRECT;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b110???: // register indirect [Rxxx]
		begin
			bi_mutex_sel = 10 + {1'b0, src[2:0]};
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b111???: // indexed [Rxxx + c16]
		begin
			bi_mutex_sel = 10 + {1'b0, src[2:0]};
			alu_enable_in1 = 1;
			
			state_next = STATE_EXECUTE_FETCH_SRC_INDEXED_0;
		end
		6'b000000,6'b100???:
		begin
			case(opcode)
			4'b0001,4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110: // MOV,ADD,SUB,AND,OR,ADDC,CMP
			begin
				state_next = STATE_EXECUTE_FETCH_DST;
			end
			4'b1010,4'b1011,4'b1100,4'b1111: // JMP,CALL,RET,JXX
			begin
				state_next = STATE_EXECUTE_EXECUTE;
			end
			endcase
		end
		default:
			state_next = STATE_INVALID_OP;
		endcase
	end
	STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		regs_enable_op_src_write = 1;
		
		case(opcode)
		4'b1010,4'b1011: // JMP,CALL
		begin
			state_next = STATE_EXECUTE_EXECUTE;
		end
		default:
			state_next = STATE_EXECUTE_FETCH_DST;
		endcase
	end
	STATE_EXECUTE_FETCH_SRC_INDIRECT:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		mem_enable_address_in = 1;
			
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_FETCH_SRC_INDEXED_0:
	begin
		bi_mutex_sel = 22; // IR2
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_FETCH_SRC_INDEXED_1;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_FETCH_SRC_INDEXED_1:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		mem_enable_address_in = 1;
			
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_FETCH_SRC_MEM_RETRIEVE;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_FETCH_DST:
	begin
		casex(dst)
		6'b001000: // direct mode [c16]
		begin
			bi_mutex_sel = 21; // IR1
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b011000: // indirect mode [[c16]]
		begin
			bi_mutex_sel = 21; // IR1
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_DST_INDIRECT;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b110???: // register indirect [Rxxx]
		begin
			bi_mutex_sel = 10 + {1'b0, dst[2:0]};
			mem_enable_address_in = 1;
			
			mem_op_read = 1;
			
			state_saved = STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE;
			state_next = STATE_WAIT_OP_FINISH;
		end
		6'b111???: // indexed [Rxxx + c16]
		begin
			bi_mutex_sel = 10 + {1'b0, dst[2:0]};
			alu_enable_in1 = 1;
			
			state_next = STATE_EXECUTE_FETCH_DST_INDEXED_0;
		end
		6'b000000,6'b100???:
			state_next = STATE_EXECUTE_EXECUTE;
		default:
			state_next = STATE_INVALID_OP;
		endcase
	end
	STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		regs_enable_op_dst_write = 1;
		
		state_next = STATE_EXECUTE_EXECUTE;
	end
	STATE_EXECUTE_FETCH_DST_INDIRECT:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		mem_enable_address_in = 1;
			
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_FETCH_DST_INDEXED_0:
	begin
		bi_mutex_sel = 21; // IR1
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_FETCH_DST_INDEXED_1;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_FETCH_DST_INDEXED_1:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		mem_enable_address_in = 1;
			
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_FETCH_DST_MEM_RETRIEVE;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_EXECUTE:
	begin
		case(opcode)
		4'b0001: // MOV
		begin
			casex(dst)
			6'b100???: // to REG
			begin
				case(dst[2:0])
				3'b000:
					regs_enable_r0_write = 1;
				3'b001:
					regs_enable_r1_write = 1;
				3'b010:
					regs_enable_r2_write = 1;
				3'b011:
					regs_enable_r3_write = 1;
				3'b100:
					regs_enable_r4_write = 1;
				3'b101:
					regs_enable_r5_write = 1;
				3'b110:
					regs_enable_r6_write = 1;
				3'b111:
					regs_enable_r7_write = 1;
				endcase
				
				casex(src)
				6'b100???: // from REG
				begin
					bi_mutex_sel = 10 + {1'b0, src[2:0]};
				end
				6'b000000: // from c16
				begin
					bi_mutex_sel = 22; // IR2
				end
				6'b001000,6'b011000,6'b110???,6'b111???:
				begin
					bi_mutex_sel = 30; // OP_SRC
				end
				endcase
				
				state_next = STATE_FETCH;
			end
			6'b001000,6'b011000,6'b110???,6'b111???:
			begin
				regs_enable_op_dst_write = 1;
				
				casex(src)
				6'b100???: // from REG
				begin
					bi_mutex_sel = 10 + {1'b0, src[2:0]};
				end
				6'b000000: // from c16
				begin
					bi_mutex_sel = 22; // IR2
				end
				6'b001000,6'b011000,6'b110???,6'b111???:
				begin
					bi_mutex_sel = 30; // OP_SRC
				end
				endcase
				
				casex(dst)
				6'b001000:
					state_next = STATE_EXECUTE_STORE_DIRECT;
				6'b011000:
					state_next = STATE_EXECUTE_STORE_INDIRECT;
				6'b110???:
					state_next = STATE_EXECUTE_STORE_REG_DIRECT;
				6'b111???:
					state_next = STATE_EXECUTE_STORE_REG_INDEXED;
				default:
					state_next = STATE_INVALID_OP;
				endcase
			end
			endcase
		end
		4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110: // ADD,SUB,AND,OR,ADDC,CMP
		begin
			casex(src)
			6'b100???:
				bi_mutex_sel = 10 + {1'b0, src[2:0]};
			6'b000000:
				bi_mutex_sel = 22; // IR2
			6'b001000,6'b011000,6'b110???,6'b111???:
				bi_mutex_sel = 30;
			endcase
			
			alu_enable_in2 = 1;
			
			state_next = STATE_EXECUTE_ALU_OP_0;
		end
		4'b1000: // NEG
		begin
			casex(dst)
			6'b100???:
				bi_mutex_sel = 10 + {1'b0, dst[2:0]};
			6'b000000:
				bi_mutex_sel = 21; // IR1
			6'b001000,6'b011000,6'b110???,6'b111???:
				bi_mutex_sel = 31;
			endcase
			
			alu_enable_in2 = 1;
			
			state_next = STATE_EXECUTE_ALU_OP_0;
		end
		4'b1001: // NOT
		begin
			casex(dst)
			6'b100???:
				bi_mutex_sel = 10 + {1'b0, dst[2:0]};
			6'b000000:
				bi_mutex_sel = 21; // IR1
			6'b001000,6'b011000,6'b110???,6'b111???:
				bi_mutex_sel = 31;
			endcase
			
			alu_enable_in1 = 1;
			
			alu_op_not = 1;
			
			state_saved = STATE_EXECUTE_STORE_N;
			state_next = STATE_WAIT_OP_FINISH;
		end
		4'b1010: // JMP
		begin
			casex(src)
			6'b100???:
				bi_mutex_sel = 10 + {1'b0, src[2:0]};
			6'b000000:
				bi_mutex_sel = 22; // IR2
			6'b001000,6'b011000,6'b110???,6'b111???:
				bi_mutex_sel = 30;
			endcase
			
			regs_enable_pc_write = 1;
			
			state_next = STATE_FETCH;
		end
		4'b1011: // CALL
		begin
			bi_mutex_sel = 19; // SP
			mem_enable_address_in = 1;
			
			state_next = STATE_EXECUTE_CALL_1;
		end
		4'b1100: // RET
		begin
			bi_mutex_sel = 19; // SP
			alu_enable_in1 = 1;
			
			state_next = STATE_EXECUTE_RET_1;
		end
		4'b1111: // Jxx
		begin
			bi_mutex_sel = 20; // IR0
			signext_enable_in = 1;
			
			state_next = STATE_EXECUTE_JXX_1;
		end
		endcase
	end
	STATE_EXECUTE_RET_1:
	begin
		bi_mutex_sel = 1; // ONE
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_RET_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_RET_2:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		regs_enable_sp_write = 1;
		
		state_next = STATE_EXECUTE_RET_3;
	end
	STATE_EXECUTE_RET_3:
	begin
		bi_mutex_sel = 19; // SP
		mem_enable_address_in = 1;
		
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_RET_4;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_RET_4:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		regs_enable_pc_write = 1;
		
		state_next = STATE_FETCH;
	end
	STATE_EXECUTE_CALL_1:
	begin
		bi_mutex_sel = 2; // PC
		mem_enable_data_in = 1;
		
		mem_op_write = 1;
		
		state_saved = STATE_EXECUTE_CALL_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_CALL_2:
	begin
		bi_mutex_sel = 19; // SP
		alu_enable_in1 = 1;
		
		state_next = STATE_EXECUTE_CALL_3;
	end
	STATE_EXECUTE_CALL_3:
	begin
		bi_mutex_sel = 1; // ONE
		alu_enable_in2 = 1;
		
		alu_op_sub = 1;
		
		state_saved = STATE_EXECUTE_CALL_4;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_CALL_4:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		regs_enable_sp_write = 1;
		
		state_next = STATE_EXECUTE_CALL_5;
	end
	STATE_EXECUTE_CALL_5:
	begin
		casex(src)
		6'b100???:
			bi_mutex_sel = 10 + {1'b0, src[2:0]};
		6'b000000:
			bi_mutex_sel = 22; // IR2
		6'b001000,6'b011000,6'b110???,6'b111???:
			bi_mutex_sel = 30;
		endcase
		
		regs_enable_pc_write = 1;
		
		state_next = STATE_FETCH;
	end
	STATE_EXECUTE_JXX_1:
	begin
		bi_mutex_sel = 2; // PC
		alu_enable_in1 = 1;
		
		state_next = STATE_EXECUTE_JXX_2;
	end
	STATE_EXECUTE_JXX_2:
	begin
		bi_mutex_sel = 18; // SIGNEXT
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_JXX_3;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_JXX_3:
	begin
		case(opcode2)
		4'b0001: // JE
		begin
			if(z)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b1001: // JNE
		begin
			if(~z)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0010: // JLE
		begin
			if(z | (n ^ v))
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b1010: // JG
		begin
			if(~(z | (n ^ v)))
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0011: // JL
		begin
			if(n ^ v)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b1011: // JGE
		begin
			if(~(n ^ v))
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0100: // JLEU
		begin
			if(c | z)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b1100: // JGU
		begin
			if(~(c | z))
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0101: // JCS
		begin
			if(c)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0110: // JNEG
		begin
			if(n)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		4'b0111: // JVS
		begin
			if(v)
			begin
				bi_mutex_sel = 3; // ALU_OUT
				regs_enable_pc_write = 1;
			end
		end
		endcase
		
		state_next = STATE_FETCH;
	end
	STATE_EXECUTE_ALU_OP_0:
	begin
		case(opcode)
		4'b0010,4'b0011,4'b0100,4'b0101,4'b1101,4'b0110: // ADD,SUB,AND,OR,ADDC,CMP
		begin
			casex(dst)
			6'b100???:
				bi_mutex_sel = 10 + {1'b0, dst[2:0]};
			6'b000000:
				bi_mutex_sel = 21; // IR1
			6'b001000,6'b011000,6'b110???,6'b111???:
				bi_mutex_sel = 31; // OP_DST
			endcase
			
			alu_enable_in1 = 1;
			
			alu_op_add = (opcode == 4'b0010) | (opcode == 4'b1101);
			alu_op_sub = (opcode == 4'b0011) | (opcode == 4'b0110);
			alu_op_and = (opcode == 4'b0100);
			alu_op_or = (opcode == 4'b0101);
		
			case(opcode)
			4'b0010,4'b0011,4'b0110: // ADD,SUB,CMP
			begin
				state_saved = STATE_EXECUTE_STORE_C;
				state_next = STATE_WAIT_OP_FINISH;
			end
			4'b0100,4'b0101: // AND,OR
			begin	
				state_saved = STATE_EXECUTE_STORE_N;
				state_next = STATE_WAIT_OP_FINISH;
			end
			4'b1101: // ADDC
			begin	
				state_saved = STATE_EXECUTE_ALU_ADDC;
				state_next = STATE_WAIT_OP_FINISH;
			end
			endcase
		end
		4'b1000: // NEG
		begin
			bi_mutex_sel = 0;
			alu_enable_in1 = 1;
			
			alu_op_sub = 1;
			
			state_saved = STATE_EXECUTE_STORE_C;
			state_next = STATE_WAIT_OP_FINISH;
		end
		endcase
	end
	STATE_EXECUTE_ALU_ADDC:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		alu_enable_in1 = 1;
		
		state_next = STATE_EXECUTE_ALU_ADDC_2;
	end
	STATE_EXECUTE_ALU_ADDC_2:
	begin
		bi_mutex_sel = 5; // Sign extended ALU_C
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_STORE_C;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_STORE_C:
	begin
		bi_mutex_sel = 5; // ALU_C
		regs_enable_c_write = 1;
		
		state_next = STATE_EXECUTE_STORE_V;
	end
	STATE_EXECUTE_STORE_V:
	begin
		bi_mutex_sel = 7; // ALU_V
		regs_enable_v_write = 1;
		
		state_next = STATE_EXECUTE_STORE_N;
	end
	STATE_EXECUTE_STORE_N:
	begin
		bi_mutex_sel = 6; // ALU_N
		regs_enable_n_write = 1;
		
		state_next = STATE_EXECUTE_STORE_Z;
	end
	STATE_EXECUTE_STORE_Z:
	begin
		bi_mutex_sel = 4; // ALU_Z
		regs_enable_z_write = 1;
		
		state_next = STATE_EXECUTE_ALU_STORE;
	end
	STATE_EXECUTE_ALU_STORE:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		
		casex(dst)
		6'b100???:
		begin
			case(dst[2:0])
			3'b000:
				regs_enable_r0_write = 1;
			3'b001:
				regs_enable_r1_write = 1;
			3'b010:
				regs_enable_r2_write = 1;
			3'b011:
				regs_enable_r3_write = 1;
			3'b100:
				regs_enable_r4_write = 1;
			3'b101:
				regs_enable_r5_write = 1;
			3'b110:
				regs_enable_r6_write = 1;
			3'b111:
				regs_enable_r7_write = 1;
			endcase
			
			state_next = STATE_FETCH;
		end
		6'b001000:
		begin
			regs_enable_op_dst_write = 1;
			state_next = STATE_EXECUTE_STORE_DIRECT;
		end
		6'b011000:
		begin
			regs_enable_op_dst_write = 1;
			state_next = STATE_EXECUTE_STORE_INDIRECT;
		end
		6'b110???:
		begin
			regs_enable_op_dst_write = 1;
			state_next = STATE_EXECUTE_STORE_REG_DIRECT;
		end
		6'b111???:
		begin
			regs_enable_op_dst_write = 1;
			state_next = STATE_EXECUTE_STORE_REG_INDEXED;
		end
		6'b000000:
		begin
			if(opcode == 4'b0110) // CMP
				state_next = STATE_FETCH;
			else
				state_next = STATE_INVALID_OP;
		end
		default:
			state_next = STATE_INVALID_OP;
		endcase
	end
	STATE_EXECUTE_STORE_DIRECT:
	begin
		bi_mutex_sel = 21; // IR1
		mem_enable_address_in = 1;
		
		state_next = STATE_EXECUTE_STORE_DIRECT_2;
	end
	STATE_EXECUTE_STORE_DIRECT_2:
	begin
		bi_mutex_sel = 31; // OP_DST
		mem_enable_data_in = 1;
		
		mem_op_write = 1;
		
		state_saved = STATE_FETCH;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_STORE_INDIRECT:
	begin
		bi_mutex_sel = 21; // IR1
		mem_enable_address_in = 1;
		
		mem_op_read = 1;
		
		state_saved = STATE_EXECUTE_STORE_INDIRECT_2;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_STORE_INDIRECT_2:
	begin
		bi_mutex_sel = 8; // MEM_DATA
		mem_enable_address_in = 1;
		
		state_next = STATE_EXECUTE_STORE_DIRECT_2;
	end
	STATE_EXECUTE_STORE_REG_DIRECT:
	begin
		bi_mutex_sel = 10 + {1'b0, dst[2:0]}; // Rxxx
		mem_enable_address_in = 1;
		
		state_next = STATE_EXECUTE_STORE_DIRECT_2;
	end
	STATE_EXECUTE_STORE_REG_INDEXED:
	begin
		bi_mutex_sel = 10 + {1'b0, dst[2:0]}; // Rxxx
		alu_enable_in1 = 1;
		
		state_next = STATE_EXECUTE_STORE_REG_INDEXED_2;
	end
	STATE_EXECUTE_STORE_REG_INDEXED_2:
	begin
		bi_mutex_sel = 21; // IR1
		alu_enable_in2 = 1;
		
		alu_op_add = 1;
		
		state_saved = STATE_EXECUTE_STORE_REG_INDEXED_3;
		state_next = STATE_WAIT_OP_FINISH;
	end
	STATE_EXECUTE_STORE_REG_INDEXED_3:
	begin
		bi_mutex_sel = 3; // ALU_OUT
		mem_enable_address_in = 1;
		
		state_next = STATE_EXECUTE_STORE_DIRECT_2;
	end
	STATE_WAIT_OP_FINISH:
		state_next = state_saved;
	STATE_INVALID_OP: // On invalid operation stop execution
		state_next = STATE_INVALID_OP;
	endcase
end

// Detection of invalid operation
always@*
begin
	case(opcode)
	4'b0001,4'b0010,4'b0011,4'b0100,4'b0101,4'b1101: // If op is MOV,ADD,SUB,AND,OR,ADDC
	begin
		invalid_op_reg = 1'b0;
		if(dst == 6'b000000) // If dest is immediate, then invalid op
			invalid_op_reg = 1'b1;
	end
	4'b0110: // If op is CMP
		invalid_op_reg = 1'b0;
	4'b1000,4'b1001: // If op is NEG,NOT
	begin
		invalid_op_reg = 1'b0;
		if(src != 6'b000000) // If src is not 6'b000000 then invalid op
			invalid_op_reg = 1'b1;
	end
	4'b1010,4'b1011: // If op is JMP,CALL
	begin
		invalid_op_reg = 1'b0;
		if(dst != 6'b000000) // If dest is not 6'b000000 then invalid op
			invalid_op_reg = 1'b1;
	end
	4'b1100: // If op is RET
	begin
		invalid_op_reg = 1'b0;
		if(dst != 6'b000000 || src != 6'b000000) // If rest of op is not 0 then invalid op
			invalid_op_reg = 1'b1;
	end
	4'b1111:
	begin
		case(opcode2)
		4'b0001,4'b1001,4'b0010,4'b1010,4'b0011,4'b1011,4'b0100,4'b1100,4'b0101,4'b0110,4'b0111:
			invalid_op_reg = 1'b0;
		default:
			invalid_op_reg = 1'b1;
		endcase
	end
	default:
		invalid_op_reg = 1'b1;
	endcase
end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:17:29 05/15/2015 
// Design Name: 
// Module Name:    alu_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu_wrapper(
	input clk,
	input [15:0] ORGA1_BUS,
	// Operations implemented by the module
	input op_add,
	input op_sub,
	input op_and,
	input op_or,
	input op_not,
	// Signals to control registers inputs and outputs
	input enable_in1,
	input enable_in2,
	
	output [15:0] out,
	output z,
	output c,
	output v,
	output n,
	// Async reset
	input reset
    );

wire we = op_add | op_sub | op_and | op_or | op_not;
wire [4:0] op_to_alu;

rw_register #(.N(5)) OP (
	.clk(clk),
	.write_enable(we),
	.data_in({op_add,op_sub,op_and,op_or,op_not}),
	.data_out(op_to_alu),
	.reset(reset)
); 

wire [15:0] alu_in_a_to_alu;
rw_register ALU_IN_A(
	.clk(clk),
	.write_enable(enable_in1),
	.data_in(ORGA1_BUS),
	.data_out(alu_in_a_to_alu),
	.reset(reset)
);

wire [15:0] alu_in_b_to_alu;
rw_register ALU_IN_B(
	.clk(clk),
	.write_enable(enable_in2),
	.data_in(ORGA1_BUS),
	.data_out(alu_in_b_to_alu),
	.reset(reset)
);

wire [15:0] alu_to_alu_out;
rw_register ALU_OUT(
	.clk(clk),
	.write_enable(1'b1),
	.data_in(alu_to_alu_out),
	.data_out(out),
	.reset(reset)
);

wire alu_to_z;
rw_register #(.N(1)) ALU_Z (
	.clk(clk),
	.write_enable(1'b1),
	.data_in(alu_to_z),
	.data_out(z),
	.reset(reset)
);

wire alu_to_c;
rw_register #(.N(1)) ALU_C (
	.clk(clk),
	.write_enable(1'b1),
	.data_in(alu_to_c),
	.data_out(c),
	.reset(reset)
);

wire alu_to_v;
rw_register #(.N(1)) ALU_V (
	.clk(clk),
	.write_enable(1'b1),
	.data_in(alu_to_v),
	.data_out(v),
	.reset(reset)
);

wire alu_to_n;
rw_register #(.N(1)) ALU_N (
	.clk(clk),
	.write_enable(1'b1),
	.data_in(alu_to_n),
	.data_out(n),
	.reset(reset)
);

wire [2:0] alu_op;
// {op_add,op_sub,op_and,op_or,op_not}
assign alu_op = {~op_to_alu[4] & op_to_alu[0],~op_to_alu[4] & (op_to_alu[2] | op_to_alu[1]),~op_to_alu[4] & (op_to_alu[3] | op_to_alu[1])};

alu_16b alu (
	.a(alu_in_a_to_alu),
	.b(alu_in_b_to_alu),
	.out(alu_to_alu_out),
	.alu_op(alu_op),
	.z(alu_to_z),
	.n(alu_to_n),
	.c(alu_to_c),
	.v(alu_to_v)
);

endmodule

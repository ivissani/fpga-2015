`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:25:10 05/20/2015 
// Design Name: 
// Module Name:    mmu_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mmu_wrapper(
	input clk,
	input reset,
	// External interface to memory
	output MEM_OE,
	output MEM_WE,
	output MEM_CLK,
	output MEM_ADV,
	output MEM_CE,
	output MEM_UB,
	output MEM_LB,
	output MEM_CRE,
	output [22:0] MEM_ADDR,
	inout [15:0] MEM_DATA,
	// ORGA1 BUS
	input [15:0] ORGA1_BUS,
	// Registers control signals
	input enable_address_in,
	input enable_data_in,
	output [15:0] data_out,
	// Operation constrol signals
	input op_read,
	input op_write,
	output mem_rdy
    );

wire memory_user_start;	// This signal HIGH indicates that the operation can be started
wire memory_user_rw;		// This signal LOW indicates a read operation. HIGH indicates write operation
wire [15:0] memory_user_address;
wire [15:0] memory_user_data_in;
wire [15:0] memory_user_data_out;
wire memory_user_ready;	// Indicates to the user 

assign memory_user_start = (op_read | op_write) & memory_user_ready; // Prevent op_star from being SET while the mem is not ready
assign memory_user_rw = (~op_read & op_write);

assign mem_rdy = memory_user_ready;

rw_register ADDRESS_IN(
	.clk(clk),
	.write_enable(enable_address_in),
	.data_in(ORGA1_BUS),
	.data_out(memory_user_address),
	.reset(reset)
);

rw_register DATA_IN(
	.clk(clk),
	.write_enable(enable_data_in),
	.data_in(ORGA1_BUS),
	.data_out(memory_user_data_in),
	.reset(reset)
);

//assign memory_user_data_in = 16'b1010101010101010;

mmu memory_unit(
	.clk(clk),
	.MEM_OE(MEM_OE),
	.MEM_WE(MEM_WE),
	.MEM_CLK(MEM_CLK),
	.MEM_ADV(MEM_ADV),
	.MEM_CE(MEM_CE),
	.MEM_UB(MEM_UB),
	.MEM_LB(MEM_LB),
	.MEM_CRE(MEM_CRE),
	.MEM_ADDR(MEM_ADDR),
	.MEM_DATA(MEM_DATA),
	.user_start(memory_user_start),
	.user_rw(memory_user_rw),
	.user_address(memory_user_address),
	.user_data_in(memory_user_data_in),
	.user_data_out(data_out),
	.user_ready(memory_user_ready)
);

endmodule

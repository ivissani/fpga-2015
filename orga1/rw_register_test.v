`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:31:52 05/13/2015
// Design Name:   rw_register
// Module Name:   /home/ivissani/git/fpga-2015/orga1/rw_register_test.v
// Project Name:  orga1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: rw_register
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module rw_register_test;

	// Inputs
	reg clk;
	reg reset;
	reg [7:0] data_in;
	reg write_enable;
	reg read_enable;

	// Outputs
	wire [7:0] data_out;

	// Instantiate the Unit Under Test (UUT)
	rw_register #(.N(8)) uut (
		.clk(clk), 
		.reset(reset), 
		.data_in(data_in), 
		.write_enable(write_enable), 
		.read_enable(read_enable), 
		.data_out(data_out)
	);
	
	always
	begin
		clk = 0;
		#5;
		clk = 1;
		#5;
	end

	initial begin
		data_in = 0;
		clk = 0;
		reset = 0;
		write_enable = 0;
		read_enable = 0;
		
		#1;
		reset = 1;
		
		#1;
		reset = 0;
		
		data_in = 15;
		write_enable = 1;
		read_enable = 1;
		
		#10;
		write_enable = 0;
		read_enable = 1;
		
		#10;
		read_enable = 0;

		#10;
		read_enable = 1;
		
		#10;
		read_enable = 0;
		
		#10;
		$stop;
	end
      
endmodule


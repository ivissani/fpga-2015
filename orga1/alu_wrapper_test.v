`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:25:28 05/15/2015
// Design Name:   alu_wrapper
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/orga1/alu_wrapper_test.v
// Project Name:  orga1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: alu_wrapper
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module alu_wrapper_test;

	// Inputs
	reg clk;
	reg op_add;
	reg op_sub;
	reg op_and;
	reg op_or;
	reg op_not;
	reg enable_in1;
	reg enable_in2;
	reg enable_out;
	reg enable_z;
	reg enable_c;
	reg enable_v;
	reg enable_n;

	// Bidirs
	wire [15:0] ORGA1_BUS;
	
	reg [15:0] REG_DATA1;
	reg [15:0] REG_DATA2;
	
	rw_register myreg1 (
		.clk(clk),
		.data_in(REG_DATA1),
		.write_enable(1'b1),
		.data_out(ORGA1_BUS)
	);
	
	rw_register myreg2 (
		.clk(clk),
		.data_in(REG_DATA2),
		.write_enable(1'b1),
		.read_enable(enable_in2),
		.data_out(ORGA1_BUS)
	);

	// Instantiate the Unit Under Test (UUT)
	alu_wrapper uut (
		.clk(clk), 
		.ORGA1_BUS(ORGA1_BUS), 
		.op_add(op_add), 
		.op_sub(op_sub), 
		.op_and(op_and), 
		.op_or(op_or), 
		.op_not(op_not), 
		.enable_in1(enable_in1), 
		.enable_in2(enable_in2), 
		.enable_out(enable_out), 
		.enable_z(enable_z), 
		.enable_c(enable_c), 
		.enable_v(enable_v), 
		.enable_n(enable_n)
	);

	always
	begin
		clk = 0;
		#5;
		clk = 1;
		#5;
	end

	integer i, j;

	initial begin
		REG_DATA1 = 0;
		REG_DATA2 = 0;
		// Initialize Inputs
		op_add = 0;
		op_sub = 0;
		op_and = 0;
		op_or = 0;
		op_not = 0;
		enable_in1 = 0;
		enable_in2 = 0;
		enable_out = 0;
		enable_z = 0;
		enable_c = 0;
		enable_v = 0;
		enable_n = 0;
		
		#10;
		
		for(i = -16; i < 16; i = i + 1)
			for(j = -16; j < 16; j = j + 1)
			begin
				// Load first operand
				REG_DATA1 = i;
				REG_DATA2 = j;
				#10;
				
				enable_in1 = 1;
				#10;
				enable_in1 = 0;
				
				// Load second operand
				enable_in2 = 1;
				#10;
				enable_in2 = 0;
				
				// Operation
				op_add = 1;
				#10;
				op_add = 0;
				
				// Connect out to bus
				enable_out = 1;
				#10;
				enable_out=0;
			end


		REG_DATA1 = 16'h0030;
		REG_DATA2 = 16'h00F0;
		#10;
		
		enable_in1 = 1;
		#10;
		enable_in1 = 0;
		
		// Load second operand
		enable_in2 = 1;
		#10;
		enable_in2 = 0;
		
		// Operation
		op_sub = 1;
		#10;
		op_sub = 0;
		
		// Connect out to bus
		enable_out = 1;
		#10;
		enable_out=0;
		
		// Wait 100 ns for global reset to finish
		$stop;
        
		// Add stimulus here

	end
      
endmodule


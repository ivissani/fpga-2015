`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   16:09:59 07/09/2015
// Design Name:   orga1_package
// Module Name:   C:/Users/Ignacio/Documents/fpga-2015/orga1/orga1_test.v
// Project Name:  orga1
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: orga1_package
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module orga1_test;

	// Inputs
	reg clk;
	reg reset;
	reg [7:0] address;

	// Outputs
	wire MEM_OE;
	wire MEM_WE;
	wire MEM_CLK;
	wire MEM_ADV;
	wire MEM_CE;
	wire MEM_UB;
	wire MEM_LB;
	wire MEM_CRE;
	wire [22:0] MEM_ADDR;
	wire [7:0] sseg;
	wire [3:0] an;
	wire mem_ready;
	wire mem_en_address_in;
	wire mem_en_data_in;
	wire mem_read;
	wire mem_write;

	// Bidirs
	wire [15:0] MEM_DATA;

	// Instantiate the Unit Under Test (UUT)
	orga1_package uut (
		.clk(clk), 
		.reset(reset), 
		.MEM_OE(MEM_OE), 
		.MEM_WE(MEM_WE), 
		.MEM_CLK(MEM_CLK), 
		.MEM_ADV(MEM_ADV), 
		.MEM_CE(MEM_CE), 
		.MEM_UB(MEM_UB), 
		.MEM_LB(MEM_LB), 
		.MEM_CRE(MEM_CRE), 
		.MEM_ADDR(MEM_ADDR), 
		.MEM_DATA(MEM_DATA), 
		.sseg(sseg), 
		.an(an), 
		.address(address), 
		.mem_ready(mem_ready), 
		.mem_en_address_in(mem_en_address_in), 
		.mem_en_data_in(mem_en_data_in), 
		.mem_read(mem_read), 
		.mem_write(mem_write)
	);
	
	// Clock simulation
	always
	begin
		clk = 0;
		#5;
		clk = 1;
		#5;
	end
	
	reg [15:0] memory[0:2**16-1];
	reg [15:0] mem_out;
	
	// Memory simulation
	always@(posedge clk)
	begin
		if(~MEM_CE)
		begin
			if(~MEM_WE) // It's a write operation
			begin
				memory[MEM_ADDR[15:0]] <= MEM_DATA;
			end
			else if(~MEM_OE) // It's a read operation
			begin
				mem_out <= memory[MEM_ADDR[15:0]];
			end
		end
	end
	
	assign MEM_DATA = (MEM_WE)?mem_out:16'bz;

	initial begin
		$readmemh("C:\\Users\\Ignacio\\Documents\\fpga-2015\\memory.h", memory);
		
		// Initialize Inputs
		clk = 0;
		reset = 0;
		address = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		#100000;
		
		$stop;
        
		// Add stimulus here

	end
      
endmodule


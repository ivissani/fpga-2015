`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:33:13 05/29/2015 
// Design Name: 
// Module Name:    bus_input_mux 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module bus_input_mux(
	input [15:0] PC,
	input [15:0] ALU_OUT,
	input ALU_Z,
	input ALU_C,
	input ALU_N,
	input ALU_V,
	input [15:0] MEM_DATA,
	input [15:0] R0,
	input [15:0] R1,
	input [15:0] R2,
	input [15:0] R3,
	input [15:0] R4,
	input [15:0] R5,
	input [15:0] R6,
	input [15:0] R7,
	input [15:0] OP_SRC,
	input [15:0] OP_DST,
	input [15:0] IR0,
	input [15:0] IR1,
	input [15:0] IR2,
	input [15:0] SP,
	input [15:0] SIGN_EXT,
	input [7:0] manual_address,
	input [4:0] sel,
	output reg [15:0] out 
    );
	 
always@*
begin
	case(sel)
	0:
		out = 0;
	1:
		out = 1;
	2:
		out = PC;
	3:
		out = ALU_OUT;
	4:
		out = {15'b0, ALU_Z};
	5:
		out = {15'b0, ALU_C};
	6:
		out = {15'b0, ALU_N};
	7:
		out = {15'b0, ALU_V};
	8:
		out = MEM_DATA;
	9:
		out = {8'b0, manual_address};
	10:
		out = R0;
	11:
		out = R1;
	12:
		out = R2;
	13:
		out = R3;
	14:
		out = R4;
	15:
		out = R5;
	16:
		out = R6;
	17:
		out = R7;
	18:
		out = SIGN_EXT;
	19:
		out = SP;
	20:
		out = IR0;
	21:
		out = IR1;
	22:
		out = IR2;
	30:
		out = OP_SRC;
	31:
		out = OP_DST;
	default:
		out = 0;
	endcase
end


endmodule

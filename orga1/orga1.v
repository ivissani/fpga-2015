`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:18:36 05/13/2015 
// Design Name: 
// Module Name:    orga1 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module orga1(
	input clk,
	input reset,
	input [15:0] ORGA1_BUS,
	
	input alu_op_add,
	input alu_op_sub,
	input alu_op_and,
	input alu_op_or,
	input alu_op_not,

	input alu_enable_in1,
	input alu_enable_in2,
	output [15:0] alu_out,
	output alu_z,
	output alu_c,
	output alu_v,
	output alu_n
    );

alu_wrapper alu (
	.clk(clk),
	.ORGA1_BUS(ORGA1_BUS),
	// Operation signals
	.op_add(alu_op_add),
	.op_sub(alu_op_sub),
	.op_and(alu_op_and),
	.op_or(alu_op_or),
	.op_not(alu_op_not),
	// Register control signals
	.enable_in1(alu_enable_in1),
	.enable_in2(alu_enable_in2),
	.out(alu_out),
	.z(alu_z),
	.c(alu_c),
	.v(alu_v),
	.n(alu_n),
	.reset(reset)
);

endmodule

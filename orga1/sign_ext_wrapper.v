`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:46:09 06/05/2015 
// Design Name: 
// Module Name:    sign_ext_wrapper 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sign_ext_wrapper(
	input clk,
	input reset,
	input [7:0] data_in,
	input enable_data_in,
	output[15:0] data_out
    );

wire [7:0] data_in_to_signext;
wire [15:0] signext_to_data_out;

rw_register #(.N(8),.INIT(8'b0)) SIGNEXT_IN (
	.clk(clk),
	.reset(reset),
	.data_in(data_in),
	.write_enable(enable_data_in),
	.data_out(data_in_to_signext)
);

sign_ext_8_to_16 signext(
	.data_in(data_in_to_signext),
	.data_out(data_out)
);

endmodule

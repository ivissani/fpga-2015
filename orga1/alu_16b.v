`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:38:39 05/13/2015 
// Design Name: 
// Module Name:    alu_16b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module alu_16b(
	input [15:0] a,
	input [15:0] b,
	input [2:0] alu_op,
	output [15:0] out,
	output c,
	output reg z,
	output reg n,
	output reg v
    );
	 
localparam OP_ADD = 3'b000;
localparam OP_SUB = 3'b001;
localparam OP_AND = 3'b010;
localparam OP_OR = 3'b011;
localparam OP_NOT = 3'b101;

reg [16:0] out_c;
wire [16:0] sign_ext_a = {a[15], a};
wire [16:0] sign_ext_b = {b[15], b};

assign {c, out} = out_c;

always@*
begin
	case(alu_op)
	OP_ADD: begin
		out_c = sign_ext_a + sign_ext_b;
		v = (~a[15] & ~b[15] & out[15]) | (a[15] & b[15] & ~out[15]);
	end
	OP_SUB: begin
		out_c = sign_ext_a + (-sign_ext_b);
		v = (~a[15] & b[15] & out[15]) | (a[15] & ~b[15] & ~out[15]);
	end
	OP_AND: begin
		out_c = {1'b0, a & b};
		v = 0;
	end
	OP_OR: begin
		out_c = {1'b0, a | b};
		v = 0;
	end
	OP_NOT: begin
		out_c = {1'b0, ~a};
		v = 0;
	end
	default: begin
		out_c = 17'b0;
		v = 0;
	end
	endcase
	z = (out == 0);
	n = out[15];
end

endmodule

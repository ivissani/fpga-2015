`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    16:52:15 05/13/2015 
// Design Name: 
// Module Name:    rw_register 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rw_register#(parameter N=16, parameter INIT=16'b0)
(
	input clk,
	input reset,
	input [N-1:0] data_in,
	input write_enable,
	output wire [N-1:0] data_out
    );

reg [N-1:0] data_reg = INIT;
reg [N-1:0] data_next;

assign data_out = data_reg;

always@(posedge clk, posedge reset)
begin
	if(reset)
		data_reg <= INIT;
	else begin
		if(write_enable)
			data_reg <= data_in;
		else
			data_reg <= data_reg;
	end
end

endmodule

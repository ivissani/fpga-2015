`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:22:30 05/22/2015 
// Design Name: 
// Module Name:    constant_register 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module constant_register#(parameter N=16, parameter VALUE=0)(
	input enable,
	output [N-1:0] data_out
    );
	 
assign data_out = enable?VALUE:{(N){1'bz}};


endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:01:14 05/13/2015 
// Design Name: 
// Module Name:    mmu_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module orga1_package (
	input clk,
	input reset,
	// External interface to memory
	output MEM_OE,
	output MEM_WE,
	output MEM_CLK,
	output MEM_ADV,
	output MEM_CE,
	output MEM_UB,
	output MEM_LB,
	output MEM_CRE,
	output [22:0] MEM_ADDR,
	inout [15:0] MEM_DATA,
	output [7:0] sseg,
	output [3:0] an,
	input [7:0] address,
	output mem_ready,
	output mem_en_address_in,
	output mem_en_data_in,
	output mem_read,
	output mem_write
    );

// Data wires from registers to bus multiplexer
wire [15:0] alu_to_mux;
wire alu_z_to_mux;
wire alu_c_to_mux;
wire alu_n_to_mux;
wire alu_v_to_mux;

wire [15:0] mem_data_to_mux;

wire [15:0] signext_to_mux;

wire [15:0] pc_to_mux;
wire [15:0] sp_to_mux;

wire [15:0] ir0_to_mux;
wire [15:0] ir1_to_mux;
wire [15:0] ir2_to_mux;

wire [15:0] r0_to_mux;
wire [15:0] r1_to_mux;
wire [15:0] r2_to_mux;
wire [15:0] r3_to_mux;
wire [15:0] r4_to_mux;
wire [15:0] r5_to_mux;
wire [15:0] r6_to_mux;
wire [15:0] r7_to_mux;

wire [15:0] op_src_to_mux;
wire [15:0] op_dst_to_mux;

// Control signals
wire signext_enable_write;

wire pc_enable_write;
wire sp_enable_write;

wire ir0_enable_write;
wire ir1_enable_write;
wire ir2_enable_write;

wire r0_enable_write;
wire r1_enable_write;
wire r2_enable_write;
wire r3_enable_write;
wire r4_enable_write;
wire r5_enable_write;
wire r6_enable_write;
wire r7_enable_write;

wire op_src_enable_write;
wire op_dst_enable_write;

wire [4:0] bi_mutex_sel;

wire [15:0] ORGA1_BUS;

bus_input_mux bi_mux (
	.PC(pc_to_mux),
	.SP(sp_to_mux),
	// instruction register
	.IR0(ir0_to_mux),
	.IR1(ir1_to_mux),
	.IR2(ir2_to_mux),
	// General purpose registers
	.R0(r0_to_mux),
	.R1(r1_to_mux),
	.R2(r2_to_mux),
	.R3(r3_to_mux),
	.R4(r4_to_mux),
	.R5(r5_to_mux),
	.R6(r6_to_mux),
	.R7(r7_to_mux),
	//
	.OP_SRC(op_src_to_mux),
	.OP_DST(op_dst_to_mux),
	// register from the ALU
	.ALU_OUT(alu_to_mux),
	.ALU_Z(alu_z_to_mux),
	.ALU_C(alu_c_to_mux),
	.ALU_N(alu_n_to_mux),
	.ALU_V(alu_v_to_mux),
	// register from the memory
	.MEM_DATA(mem_data_to_mux),
	// register from the sign extender
	.SIGN_EXT(signext_to_mux),
	.manual_address(address),
	.out(ORGA1_BUS),
	.sel(bi_mutex_sel)
);

sign_ext_wrapper signext (
	.clk(clk), .reset(reset),
	.data_in(ORGA1_BUS[7:0]),
	.enable_data_in(signext_enable_write),
	.data_out(signext_to_mux)
);

// Memory Management Unit signals
// Registers control signals
wire mem_enable_address_in;
wire mem_enable_data_in;
// Operation control signals
wire mem_op_read;
wire mem_op_write;

assign mem_en_address_in = mem_enable_address_in;
assign mem_en_data_in = mem_enable_data_in;
assign mem_read = mem_op_read;
assign mem_write = mem_op_write;

// CPU signals
// Registers control signals
wire alu_enable_in1;
wire alu_enable_in2;
// Operation control signals
wire alu_op_add;
wire alu_op_sub;
wire alu_op_and;
wire alu_op_or;
wire alu_op_not;

wire [15:0] pc_out;

// Registers
rw_register PC(
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(pc_to_mux),
	.write_enable(pc_enable_write)
);
// Instruction registers
rw_register IR0 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(ir0_to_mux),
	.write_enable(ir0_enable_write)
);
rw_register IR1 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(ir1_to_mux),
	.write_enable(ir1_enable_write)
);
rw_register IR2 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(ir2_to_mux),
	.write_enable(ir2_enable_write)
);
// Operand registers
rw_register OP_SRC (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(op_src_to_mux),
	.write_enable(op_src_enable_write)
);
rw_register OP_DST (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(op_dst_to_mux),
	.write_enable(op_dst_enable_write)
);
// Stack pointer
rw_register #(.INIT(16'hFFFE)) SP (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(sp_to_mux),
	.write_enable(sp_enable_write)
);
// Flags' registers
rw_register #(.N(1),.INIT(1'b0)) N (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS[0]),
	.data_out(n_to_cu),
	.write_enable(n_enable_write)
);
rw_register #(.N(1),.INIT(1'b0)) Z (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS[0]),
	.data_out(z_to_cu),
	.write_enable(z_enable_write)
);
rw_register #(.N(1),.INIT(1'b0)) C (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS[0]),
	.data_out(c_to_cu),
	.write_enable(c_enable_write)
);
rw_register #(.N(1),.INIT(1'b0)) V (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS[0]),
	.data_out(v_to_cu),
	.write_enable(v_enable_write)
);
// General purpose registers
rw_register R0 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r0_to_mux),
	.write_enable(r0_enable_write)
);
rw_register R1 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r1_to_mux),
	.write_enable(r1_enable_write)
);
rw_register R2 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r2_to_mux),
	.write_enable(r2_enable_write)
);
rw_register R3 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r3_to_mux),
	.write_enable(r3_enable_write)
);
rw_register R4 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r4_to_mux),
	.write_enable(r4_enable_write)
);
rw_register R5 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r5_to_mux),
	.write_enable(r5_enable_write)
);
rw_register R6 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r6_to_mux),
	.write_enable(r6_enable_write)
);
rw_register R7 (
	.clk(clk),
	.reset(reset),
	.data_in(ORGA1_BUS),
	.data_out(r7_to_mux),
	.write_enable(r7_enable_write)
);

wire [7:0] ss0;
wire [7:0] ss1;
wire [7:0] ss2;
wire [7:0] ss3;

hex_to_sseg h2ss0 (
	.hex(ORGA1_BUS[3:0]),
	.dp(1'b1),
	.sseg(ss0)
);

hex_to_sseg h2ss1 (
	.hex(ORGA1_BUS[7:4]),
	.dp(1'b1),
	.sseg(ss1)
);

hex_to_sseg h2ss2 (
	.hex(ORGA1_BUS[11:8]),
	.dp(1'b1),
	.sseg(ss2)
);

hex_to_sseg h2ss3 (
	.hex(ORGA1_BUS[15:12]),
	.dp(1'b1),
	.sseg(ss3)
);

disp_mux display (
	.clk(clk),
	.reset(reset),
	.in0(ss0),
	.in1(ss1),
	.in2(ss2),
	.in3(ss3),
	.an(an),
	.sseg(sseg)
);

mmu_wrapper memory_unit(
	.clk(clk),
	.reset(reset),
	.MEM_OE(MEM_OE),
	.MEM_WE(MEM_WE),
	.MEM_CLK(MEM_CLK),
	.MEM_ADV(MEM_ADV),
	.MEM_CE(MEM_CE),
	.MEM_UB(MEM_UB),
	.MEM_LB(MEM_LB),
	.MEM_CRE(MEM_CRE),
	.MEM_ADDR(MEM_ADDR),
	.MEM_DATA(MEM_DATA),
	.ORGA1_BUS(ORGA1_BUS),
	
	.enable_address_in(mem_enable_address_in),
	.enable_data_in(mem_enable_data_in),
	.data_out(mem_data_to_mux),
	.op_read(mem_op_read),
	.op_write(mem_op_write),
	.mem_rdy(mem_ready)
);

orga1 cpu (
	.clk(clk),
	.reset(reset),
	.ORGA1_BUS(ORGA1_BUS),
	.alu_op_add(alu_op_add),
	.alu_op_sub(alu_op_sub),
	.alu_op_and(alu_op_and),
	.alu_op_or(alu_op_or),
	.alu_op_not(alu_op_not),
	.alu_enable_in1(alu_enable_in1),
	.alu_enable_in2(alu_enable_in2),
	.alu_out(alu_to_mux),
	.alu_z(alu_z_to_mux),
	.alu_c(alu_c_to_mux),
	.alu_v(alu_v_to_mux),
	.alu_n(alu_n_to_mux)
);

control_unit uc (
	.clk(clk),
	.reset(reset),
	.bi_mutex_sel(bi_mutex_sel),
	.alu_op_add(alu_op_add),
	.alu_op_sub(alu_op_sub),
	.alu_op_and(alu_op_and),
	.alu_op_or(alu_op_or),
	.alu_op_not(alu_op_not),

	.alu_enable_in1(alu_enable_in1),
	.alu_enable_in2(alu_enable_in2),
	
	.mem_enable_address_in(mem_enable_address_in),
	.mem_enable_data_in(mem_enable_data_in),
	.mem_op_read(mem_op_read),
	.mem_op_write(mem_op_write),
	
	.signext_enable_in(signext_enable_write),
	
	.regs_enable_pc_write(pc_enable_write),
	
	.regs_enable_ir0_write(ir0_enable_write),
	.regs_enable_ir1_write(ir1_enable_write),
	.regs_enable_ir2_write(ir2_enable_write),
	.regs_enable_sp_write(sp_enable_write),
	.regs_enable_r0_write(r0_enable_write),
	.regs_enable_r1_write(r1_enable_write),
	.regs_enable_r2_write(r2_enable_write),
	.regs_enable_r3_write(r3_enable_write),
	.regs_enable_r4_write(r4_enable_write),
	.regs_enable_r5_write(r5_enable_write),
	.regs_enable_r6_write(r6_enable_write),
	.regs_enable_r7_write(r7_enable_write),
	.regs_enable_z_write(z_enable_write),
	.regs_enable_c_write(c_enable_write),
	.regs_enable_n_write(n_enable_write),
	.regs_enable_v_write(v_enable_write),
	
	.regs_enable_op_src_write(op_src_enable_write),
	.regs_enable_op_dst_write(op_dst_enable_write),
	
	.mem_ready(mem_ready),
	.z(z_to_cu),
	.c(c_to_cu),
	.v(v_to_cu),
	.n(n_to_cu),
	
	.ir0(ir0_to_mux)
);

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:50:05 06/05/2015 
// Design Name: 
// Module Name:    sign_ext_8_to_16 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sign_ext_8_to_16(
	input [7:0] data_in,
	output [15:0] data_out
    );

// Sign extension
// The upper byte must replicate the MSB of data_in
assign data_out[15] = data_in[7];
assign data_out[14] = data_in[7];
assign data_out[13] = data_in[7];
assign data_out[12] = data_in[7];
assign data_out[11] = data_in[7];
assign data_out[10] = data_in[7];
assign data_out[9] = data_in[7];
assign data_out[8] = data_in[7];

// The lower byte is the same
assign data_out[7:0] = data_in;

endmodule

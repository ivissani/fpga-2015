`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:17:39 04/24/2015 
// Design Name: 
// Module Name:    parametric_clk_divisor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module parametric_clk_divisor#(parameter CYCLES=50000000) // 1Hz on a 100MHz clock
(
    input wire clk,
	 input wire reset,
    output reg clk_out
    );

reg [31:0] counter;
reg [31:0] counter_next;
reg clk_next;
wire [31:0] cycles = CYCLES;

always@(posedge clk, posedge reset) begin
	if(reset) begin
		counter <= 0;
		clk_out <= 0;
	end
	else begin
		counter <= counter_next;
		clk_out <= clk_next;
	end
end

always@* begin
	clk_next = clk_out;
	counter_next = counter + 1;
	if(counter_next == cycles) begin
		clk_next = ~clk_out;
		counter_next = 0;
	end
end



endmodule

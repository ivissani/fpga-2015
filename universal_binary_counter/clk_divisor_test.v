`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:09:46 04/17/2015
// Design Name:   clk_divisor
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/universal_binary_counter/clk_divisor_test.v
// Project Name:  universal_binary_counter
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: clk_divisor
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module clk_divisor_test;

	// declaration
	localparam T=20; // clock period

	// Inputs
	reg clk;
	reg reset;

	// Outputs
	wire clk_out;

	// Instantiate the Unit Under Test (UUT)
	clk_divisor uut (
		.clk(clk), 
		.reset(reset), 
		.clk_out(clk_out)
	);

	// Simulate clock
	always
		begin
			clk = 1'b1;
			#(T/2);
			clk = 1'b0;
			#(T/2);
		end

	initial begin
		// Reset the module
		reset = 1'b1;
		#(T/2);
		reset = 1'b0;
		
		// Initialize Inputs
		clk = 0;
		
		forever
			#T;

	end
      
endmodule


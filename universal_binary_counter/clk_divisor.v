`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:59:25 04/17/2015 
// Design Name: 
// Module Name:    clk_divisor 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module clk_divisor#(
parameter N=23
)
(
	input wire clk,
	input wire reset,
	output reg clk_out
    );
	 
reg [N-1:0] q;
reg [N-1:0] q_next;
reg q_clk_next;

always@(posedge clk, posedge reset)
begin
	if(reset)
		begin
			clk_out <= 0;
			q <= 0;
		end
	else
		begin
			q <= q_next;
			clk_out <= q_clk_next;
		end
end

always@*
begin
	q_clk_next = clk_out;
	q_next = q + 1;
	if(q_next == 0)
		q_clk_next = ~clk_out;
end
	


endmodule

`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:41:40 04/17/2015
// Design Name:   ub_counter
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/universal_binary_counter/ub_counter_test.v
// Project Name:  universal_binary_counter
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: ub_counter
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module ub_counter_test;

	// declaration
	localparam T=20; // clock period
	// Inputs
	reg clk;
	reg reset;
	reg syn_clr;
	reg load;
	reg en;
	reg up;
	reg [7:0] d;

	// Outputs
	wire [7:0] q;
	wire max_tick;
	wire min_tick;

	// Instantiate the Unit Under Test (UUT)
	ub_counter uut (
		.clk(clk), 
		.reset(reset), 
		.syn_clr(syn_clr), 
		.load(load), 
		.en(en), 
		.up(up), 
		.d(d), 
		.q(q), 
		.max_tick(max_tick), 
		.min_tick(min_tick)
	);
	
	// Simulate clock
	always
		begin
			clk = 1'b1;
			#(T/2);
			clk = 1'b0;
			#(T/2);
		end
	integer count_to;

	initial begin
		// Reset the module
		reset = 1'b1;
		#(T/2);
		reset = 1'b0;
		
		en = 1;
		up = 1;
		for(count_to = 0; count_to < 128; count_to = count_to + 1)
		begin	
			#T;
		end
		up = 0;
		for(; count_to > 0; count_to = count_to - 1)
		begin	
			#T;
		end
		en = 0;
		$stop;
        
		// Add stimulus here

	end
      
endmodule


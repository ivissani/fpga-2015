`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:24:51 04/17/2015 
// Design Name: 
// Module Name:    ub_counter 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module ub_counter#(
parameter N=8 // Amount of bits of the binary counter
)
(
	input wire clk, input wire reset,
	input wire syn_clr, input wire load,
	input wire en,
	input wire up, 
	input wire [N-1:0] d,
	output reg [N-1:0] q,
	output reg [3:0] bcd0,
	output reg [3:0] bcd1,
	output reg [3:0] bcd2,
	output reg [3:0] bcd3,
	output reg max_tick, output reg min_tick
);

reg [N-1:0] q_next;

// Count also in BCD
reg [3:0] bcd0_next;
reg [3:0] bcd1_next;
reg [3:0] bcd2_next;
reg [3:0] bcd3_next;

always@(posedge clk,posedge reset)
begin
	if(reset)
		begin
			q <= 0;
			bcd0 <= 0;
			bcd1 <= 0;
			bcd2 <= 0;
			bcd3 <= 0;
		end
	else
		begin
			q <= q_next;
			bcd0 <= bcd0_next;
			bcd1 <= bcd1_next;
			bcd2 <= bcd2_next;
			bcd3 <= bcd3_next;
		end
end

always@* // Next state logic
begin
	max_tick = 1'b0;
	min_tick = 1'b0;
	
	// Default, maintain old values
	bcd0_next = bcd0;
	bcd1_next = bcd1;
	bcd2_next = bcd2;
	bcd3_next = bcd3;
	
	if(syn_clr)
		q_next = 0;
	else
		if(load)
			q_next = d;
		else
			if(en)
				if(up)
					begin
						q_next = q + 1;
						if(q_next < q)
							begin
								max_tick = 1'b1;
								bcd0_next = 0;
								bcd1_next = 0;
								bcd2_next = 0;
								bcd3_next = 0;
							end
						else
							begin
								bcd0_next = bcd0 + 1;
								if(bcd0_next == 10)
									begin
										bcd0_next = 0;
										bcd1_next = bcd1 + 1;
										if(bcd1_next == 10)
											begin
												bcd1_next = 0;
												bcd2_next = bcd2+1;
												if(bcd2_next == 10)
													begin
														bcd2_next = 0;
														bcd3_next = bcd3+1;
														if(bcd3_next == 10)
															bcd3_next = 0;
													end
											end
									end
							end
					end
				else
					begin
						q_next = q - 1;
						if(q_next > q)
							begin
								min_tick = 1'b1;
								// Hadcoded max value
								bcd0_next = 5;
								bcd1_next = 5;
								bcd2_next = 2;
								bcd3_next = 0;
							end
						else
							begin
								bcd0_next = bcd0 - 1;
								if(bcd0_next == 15)
									begin
										bcd0_next = 9;
										bcd1_next = bcd1 - 1;
										if(bcd1_next == 15)
											begin
												bcd1_next = 9;
												bcd2_next = bcd2-1;
												if(bcd2_next == 15)
													begin
														bcd2_next = 9;
														bcd3_next = bcd3-1;
														if(bcd3_next == 15)
															bcd3_next = 9;
													end
											end
									end
							end
					end
			else
				q_next = q;
end


endmodule

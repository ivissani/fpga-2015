`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:11:10 04/22/2015 
// Design Name: 
// Module Name:    main 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module main#(
parameter N=8
)
(
	input wire clk, 
	input wire reset, 
	input wire enabled, 
	input wire up,
	output wire [3:0] an,
	output wire overflow, 
	output wire underflow,
	output wire [7:0] sseg
);

wire [N-1:0] counter_output;
wire [7:0] hex_out0;
wire [7:0] hex_out1;
wire [7:0] hex_out2;

wire clk_divided;
parametric_clk_divisor clkdiv(
	.clk(clk), 
	.reset(reset), 
	.clk_out(clk_divided)
);

wire [3:0] bcd0;
wire [3:0] bcd1;
wire [3:0] bcd2;

ub_counter #(.N(N)) counter(
	.clk(clk_divided), 
	.reset(reset), 
	.syn_clr(1'b0), 
	.load(1'b0), 
	.en(enabled), 
	.up(up),
	.d(0),
	.q(counter_output),
	.max_tick(overflow),
	.min_tick(underflow),
	.bcd0(bcd0),
	.bcd1(bcd1),
	.bcd2(bcd2)
);

hex_to_sseg hex0(
	.hex(bcd0),
	.dp(1'b1),
	.sseg(hex_out0)
);

hex_to_sseg hex1(
	.hex(bcd1),
	.dp(1'b1),
	.sseg(hex_out1)
);

hex_to_sseg hex2(
	.hex(bcd2),
	.dp(1'b1),
	.sseg(hex_out2)
);


disp_mux display(
	.clk(clk),
	.reset(reset),
	.in0(hex_out0),
	.in1(hex_out1),
	.in2(hex_out2),
	.in3(8'b11111111),
	.an(an),
	.sseg(sseg)
);

endmodule

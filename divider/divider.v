`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:23:39 05/06/2015 
// Design Name: 
// Module Name:    divider 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module divider(
    input [31:0] divisor,
    input [31:0] dividendo,
    input start,
    output reg [31:0] cociente,
    output reg [31:0] resto,
    output reg ready,
    output reg done,
	 input clk
    );

localparam STATE_IDLE = 2'b00;
localparam STATE_DIVIDE = 2'b01;
localparam STATE_DONE = 2'b10;

reg [1:0] state_reg = STATE_IDLE;
reg [1:0] state_next;

reg [31:0] cociente_next;
reg [31:0] resto_next;

reg ready_next;
reg done_next;

always@(posedge clk)
begin
	ready <= ready_next;
	done <= done_next;
	cociente <= cociente_next;
	resto <= resto_next;
	
	state_reg <= state_next;
end

// L�gica combinatoria para el siguiente estado
always@*
begin
	resto_next = resto;
	cociente_next = cociente;
	done_next = done;
	ready_next = ready;
	state_next = state_reg;
	
	case(state_reg)
	STATE_IDLE: begin
		ready_next = 1;
		done_next = 0;
		
		if(start) begin
			cociente_next = 0;
			resto_next = dividendo;
			
			state_next = STATE_DIVIDE;
		end
	end
	STATE_DIVIDE: begin
		ready_next = 0;
		done_next = 0;
		
		if($signed(resto) >= $signed(divisor)) begin
			cociente_next = cociente + 1;
			resto_next = resto - divisor;
		end
		else
			state_next = STATE_DONE;
	end
	STATE_DONE: begin
		ready_next = 0;
		done_next = 1;
		
		state_next = STATE_IDLE;
	end
	endcase
end

endmodule

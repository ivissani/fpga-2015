`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:43:52 05/06/2015
// Design Name:   divider
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/divider/divider_test.v
// Project Name:  divider
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: divider
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module divider_test;

	// Inputs
	reg [31:0] divisor;
	reg [31:0] dividendo;
	reg start;
	reg clk;

	// Outputs
	wire [31:0] cociente;
	wire [31:0] resto;
	wire ready;
	wire done;

	// Instantiate the Unit Under Test (UUT)
	divider uut (
		.divisor(divisor), 
		.dividendo(dividendo), 
		.start(start), 
		.cociente(cociente), 
		.resto(resto), 
		.ready(ready), 
		.done(done), 
		.clk(clk)
	);

	integer i;

	initial begin
		// Initialize Inputs
		divisor = 3;
		dividendo = 8;
		start = 1;
		
		for(i=0; i < 6; i = i + 1) begin
			clk = 0;
			#20;
			clk = 1;
			#20;
		end
		
		$stop;
        
		// Add stimulus here

	end
      
endmodule


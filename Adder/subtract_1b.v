`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:59:54 04/10/2015 
// Design Name: 
// Module Name:    subtract_1b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module subtract_1b(
    input a,
    input b,
	 input b_in,
    output b_out,
    output out
    );

wire aminusb = a ^ b;
assign out = aminusb ^ b_in;
assign b_out = (~a & b) | (~aminusb & b_in);
endmodule

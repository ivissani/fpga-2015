`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:30:11 04/15/2015 
// Design Name: 
// Module Name:    sign_adder_testing_circuit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sign_adder_testing_circuit(
	input wire clk, reset,
	input wire [3:0] a,
	input wire [3:0] b,
	output wire [3:0] an,   // enable, 1-out-of-4 asserted low
   output wire [7:0] sseg  // led segments
    );

wire [3:0] sum;
wire [7:0] sum_to_mux;
wire [7:0] sign_to_mux;

sign_adder adder1(
	.a(a), 
	.b(b), 
	.sum(sum)
);

hex_to_sseg h2sseg(
	.hex(sum[2:0]), 
	.sseg(sum_to_mux)
);

sign_circuit sc(
	.sign(sum[3]), 
	.sseg(sign_to_mux)
);

disp_mux dm(
	.clk(clk), .reset(reset), 
	.in0(sum_to_mux), 
	.in1(sign_to_mux),
	.an(an),
	.sseg(sseg)
);

endmodule

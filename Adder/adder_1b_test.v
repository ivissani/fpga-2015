`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:10:11 04/10/2015
// Design Name:   adder_1b
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/Adder/adder_1b_test.v
// Project Name:  Adder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: adder_1b
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module adder_1b_test;

	// Inputs
	reg a;
	reg b;
	reg c_in;

	// Outputs
	wire s;
	wire c_out;

	// Instantiate the Unit Under Test (UUT)
	adder_1b uut (
		.a(a), 
		.b(b), 
		.c_in(c_in), 
		.s(s), 
		.c_out(c_out)
	);

	initial begin
		// Initialize Inputs
		a = 0;
		b = 0;
		c_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 0;
		b = 0;
		c_in = 1;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 0;
		b = 1;
		c_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 0;
		b = 1;
		c_in = 1;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 1;
		b = 0;
		c_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 1;
		b = 0;
		c_in = 1;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 1;
		b = 1;
		c_in = 0;

		// Wait 100 ns for global reset to finish
		#100;
		
		// Initialize Inputs
		a = 1;
		b = 1;
		c_in = 1;

		// Wait 100 ns for global reset to finish
		#100;
        
		// Add stimulus here

	end
      
endmodule


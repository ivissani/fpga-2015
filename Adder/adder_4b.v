`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:14:37 04/10/2015 
// Design Name: 
// Module Name:    adder_4b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module addsub_4b(
    input [3:0] a,
    input [3:0] b,
    output [3:0] sum,
	 output [3:0] sub,
    output c_out,
	 output b_out
    );

wire carry_0_1;
wire carry_1_2;
wire carry_2_3;

wire borrow_0_1;
wire borrow_1_2;
wire borrow_2_3;

addsub_1b addsub0(.a(a[0]),.b(b[0]),.sum(sum[0]),.sub(sub[0]),.c_in(1'b0),.b_in(1'b0),.c_out(carry_0_1),.b_out(borrow_0_1));
addsub_1b addsub1(.a(a[1]),.b(b[1]),.sum(sum[1]),.sub(sub[1]),.c_in(carry_0_1),.b_in(borrow_0_1),.c_out(carry_1_2),.b_out(borrow_1_2));
addsub_1b addsub2(.a(a[2]),.b(b[2]),.sum(sum[2]),.sub(sub[2]),.c_in(carry_1_2),.b_in(borrow_1_2),.c_out(carry_2_3),.b_out(borrow_2_3));
addsub_1b addsub3(.a(a[3]),.b(b[3]),.sum(sum[3]),.sub(sub[3]),.c_in(carry_2_3),.b_in(borrow_2_3),.c_out(c_out),.b_out(b_out));

endmodule

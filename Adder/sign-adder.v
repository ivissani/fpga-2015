`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    10:50:38 04/10/2015 
// Design Name: 
// Module Name:    sign-adder 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sign_adder(
    input [3:0] a,
    input [3:0] b,
    output reg [3:0] sum
    );
	 
wire [3:0] az = {1'b0, a[2:0]};
wire [3:0] bz = {1'b0, b[2:0]};

always @*
begin
		sum = 4'b0000;
		if(a[3] == b[3])
			begin
				sum[2:0] = az + bz;
				sum[3] = a[3];
			end
		else
			if(a[3]) // b - a
				if(az < bz)
					begin
						sum[2:0] = bz - az;
						sum[3] = 1'b0;
					end
				else // - (a - b)
					begin
						sum[2:0] = az - bz;
						sum[3] = 1'b1;
					end
			else // a - b
				if(bz < az)
					begin
						sum[2:0] = az - bz;
						sum[3] = 1'b0;
					end
				else // - (b - a)
					begin
						sum[2:0] = bz - az;
						sum[3] = 1'b1;
					end
end

endmodule

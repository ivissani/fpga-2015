`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   11:48:14 04/10/2015
// Design Name:   sign_adder
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/Adder/sign_adder_test.v
// Project Name:  Adder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: sign_adder
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module sign_adder_test;

	// Inputs
	reg [3:0] a;
	reg [3:0] b;

	// Outputs
	wire [3:0] sum;

	// Instantiate the Unit Under Test (UUT)
	sign_adder uut (
		.a(a), 
		.b(b), 
		.sum(sum)
	);

	integer i = 0;
	integer j = 0;
	
	initial begin
		for(i = 0; i < 16; i=i+1)
			for(j=0; j < 16; j=j+1)
				begin
					// Initialize Inputs
					a = i;
					b = j;

					// Wait 100 ns for global reset to finish
					#100;
				end
		$stop;
        
		// Add stimulus here

	end
      
endmodule


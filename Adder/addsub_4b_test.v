`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   12:22:47 04/10/2015
// Design Name:   addsub_4b
// Module Name:   //dc3.ad.dc.uba.ar/RedirectedFolders06/ivissani/My Documents/FPGA/Adder/addsub_4b_test.v
// Project Name:  Adder
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: addsub_4b
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module addsub_4b_test;

	// Inputs
	reg [3:0] a;
	reg [3:0] b;

	// Outputs
	wire [3:0] sum;
	wire [3:0] sub;
	wire c_out;
	wire b_out;

	// Instantiate the Unit Under Test (UUT)
	addsub_4b uut (
		.a(a), 
		.b(b), 
		.sum(sum), 
		.sub(sub), 
		.c_out(c_out), 
		.b_out(b_out)
	);
	
	integer i, j;

	initial begin
		for(i=0;i<16;i=i+1)
			for(j=0;j<16;j=j+1)
				begin
					// Initialize Inputs
					a = i;
					b = j;

					// Wait 100 ns for global reset to finish
					#100;
				end
        
		// Add stimulus here

	end
      
endmodule


`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:26:03 04/15/2015 
// Design Name: 
// Module Name:    sign_circuit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module sign_circuit(
    input sign,
    output reg [7:0] sseg
    );
	 
always @*
begin
	sseg = {1'b0, ~sign, 6'b111111};
end

endmodule

`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:06:55 04/10/2015 
// Design Name: 
// Module Name:    adder_1b 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module addsub_1b(
    input a,
    input b,
	 input c_in,
	 input b_in,
    output sum,
	 output sub,
    output c_out,
	 output b_out
    );

wire intermediate;
assign intermediate = a ^ b;
assign sum = intermediate ^ c_in;
assign sub = intermediate ^ b_in;
assign c_out = (a & b) | (intermediate & c_in);
assign b_out = (~a & b) | (~intermediate & b_in);

endmodule

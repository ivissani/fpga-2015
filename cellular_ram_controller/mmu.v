`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:47:37 05/08/2015 
// Design Name: 
// Module Name:    mmu 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mmu(
	input clk,
	// External interface to memory
	output reg MEM_OE,
	output reg MEM_WE,
	output reg MEM_CLK,
	output reg MEM_ADV,
	output reg MEM_CE,
	output reg MEM_UB,
	output reg MEM_LB,
	output reg MEM_CRE,
	output reg [25:0] MEM_ADDR,
	inout [15:0] MEM_DATA,
	// Internal interface to user
	input user_start,	// This signal HIGH indicates that the operation can be started
	input user_rw,		// This signal LOW indicates a read operation. HIGH indicates write operation
	input [15:0] user_address,
	input [15:0] user_data_in,
	output reg [15:0] user_data_out,
	output reg user_ready	// Indicates to the user wether the device is ready to operate
									// No operation should be submited if this signal is LOW
    );

localparam STATE_INITIALIZING = 0;
localparam STATE_IDLE = 1;
localparam STATE_READ_WAIT = 2;
localparam STATE_WRITE_WAIT = 3;
localparam STATE_READ_FINISH = 4;
localparam STATE_WRITE_FINISH = 5;

// controller state
reg [2:0] state_reg = STATE_INITIALIZING;
reg [2:0] state_next;

// statefull variables
// to the user
reg user_ready_next;
reg [15:0] user_data_out_next;

// to the memory
reg MEM_CLK_NEXT;
reg MEM_ADV_NEXT;
reg MEM_CE_NEXT;
reg MEM_WE_NEXT;
reg MEM_OE_NEXT;
reg MEM_CRE_NEXT;
reg MEM_LB_NEXT;
reg MEM_UB_NEXT;
//reg MEM_WAIT_NEXT;
reg [25:0] MEM_ADDR_NEXT;

// Counter used to wait 150us for memory initialization
reg [13:0] init_counter_reg = 0;
reg [13:0] init_counter_next = 0;

// Counter used to wait 70ns for operation to finish
reg [3:0] wait_counter_reg = 0;
reg [3:0] wait_counter_next = 0;

assign MEM_DATA = user_rw?user_data_in:16'bz;

always@(posedge clk)
begin
	
	init_counter_reg <= init_counter_next;
	wait_counter_reg <= wait_counter_next;
	user_ready <= user_ready_next;
	
	MEM_CE <= MEM_CE_NEXT;
	MEM_WE <= MEM_WE_NEXT;
	MEM_OE <= MEM_OE_NEXT;
	MEM_LB <= MEM_LB_NEXT;
	MEM_UB <= MEM_UB_NEXT;
	MEM_CLK <= MEM_CLK_NEXT;
	MEM_ADV <= MEM_ADV_NEXT;
	MEM_CRE <= MEM_CRE_NEXT;
	//MEM_WAIT <= MEM_WAIT_NEXT;
	
	MEM_ADDR <= MEM_ADDR_NEXT;
	//MEM_DATA <= MEM_DATA_NEXT;
	
	user_data_out <= user_data_out_next;
	
	// Maintain state
	state_reg <= state_next;
end

always@*
begin
	// Memory signals defaults
	MEM_CLK_NEXT = 0; 	// Low for asynchronous mode
	MEM_ADV_NEXT = 0;
	MEM_CRE_NEXT = 0;
	MEM_OE_NEXT = 1;
	MEM_CE_NEXT = 1;
	MEM_WE_NEXT = 1;
	MEM_LB_NEXT = 0;
	MEM_UB_NEXT = 0;
	//MEM_WAIT_NEXT = 1'bz;
	MEM_ADDR_NEXT = MEM_ADDR;

	// User signals defaults
	user_ready_next = 0;
	user_data_out_next = user_data_out;
	
	// Internal variables defaults
	state_next = state_reg;
	
	wait_counter_next = wait_counter_reg;
	
	init_counter_next = init_counter_reg + 1;
	// 150us = 150000ns = 15000 cycles with 100MHz clk
	if(init_counter_next == 15001)
		init_counter_next = 0;
	
	case(state_reg)
		STATE_INITIALIZING: 
			begin
				// If we were in the initialization phase
				// and the counter was reset to zero
				// we are ready to operate;
				if(init_counter_next == 0)
				begin
					state_next = STATE_IDLE;
					user_ready_next = 1;
				end
			end
		STATE_IDLE:
			begin
				user_ready_next = 1;
				if(user_start) begin
					user_ready_next = 0;
					MEM_ADDR_NEXT = {9'b000000000, user_address, 1'b0};
					if(user_rw) begin
						// This is a WRITE
						//MEM_DATA_NEXT = user_data_in;
						
						MEM_CE_NEXT = 0;
						MEM_WE_NEXT = 0;
						
						state_next = STATE_WRITE_WAIT;
					end
					else begin
						// This is a READ
						
						MEM_CE_NEXT = 0;
						MEM_OE_NEXT = 0;
						
						state_next = STATE_READ_WAIT;
					end
				end
			end
		STATE_READ_WAIT:
			begin
				MEM_CE_NEXT = 0;
				MEM_OE_NEXT = 0;
				wait_counter_next = wait_counter_reg + 1;
				if(wait_counter_next == 8) begin
					wait_counter_next = 0;
					state_next = STATE_READ_FINISH;
				end
			end
		STATE_WRITE_WAIT:
			begin
				wait_counter_next = wait_counter_reg + 1;
				
				MEM_CE_NEXT = 0;
				MEM_WE_NEXT = 0;
				
				if(wait_counter_next == 8) begin
					wait_counter_next = 0;
					//MEM_DATA_NEXT = MEM_DATA;
					state_next = STATE_WRITE_FINISH;
				end
			end
		STATE_READ_FINISH:
			begin
				user_ready_next = 1;
				
				user_data_out_next = MEM_DATA;
				
				state_next = STATE_IDLE;
			end
		STATE_WRITE_FINISH:
			begin
				user_ready_next = 1;
				state_next = STATE_IDLE;
			end
	endcase
end

endmodule

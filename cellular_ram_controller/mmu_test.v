`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    11:01:14 05/13/2015 
// Design Name: 
// Module Name:    mmu_test 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module mmu_test(
	input clk,
	// External interface to memory
	output MEM_OE,
	output MEM_WE,
	output MEM_CLK,
	output MEM_ADV,
	output MEM_CE,
	output MEM_UB,
	output MEM_LB,
	output MEM_CRE,
	output [25:0] MEM_ADDR,
	inout [15:0] MEM_DATA,
	output [3:0] an,
	output [7:0] sseg,
	input [7:0] read_addr,
	output [7:0] operations
    );

reg user_start;	// This signal HIGH indicates that the operation can be started
reg user_rw;		// This signal LOW indicates a read operation. HIGH indicates write operation
reg [15:0] user_address;
reg [15:0] user_data_in;
wire [15:0] user_data_out;
wire user_ready;	// Indicates to the user 

mmu memory_unit(
	.clk(clk),
	.MEM_OE(MEM_OE),
	.MEM_WE(MEM_WE),
	.MEM_CLK(MEM_CLK),
	.MEM_ADV(MEM_ADV),
	.MEM_CE(MEM_CE),
	.MEM_UB(MEM_UB),
	.MEM_LB(MEM_LB),
	.MEM_CRE(MEM_CRE),
	.MEM_ADDR(MEM_ADDR),
	.MEM_DATA(MEM_DATA),
	.user_start(user_start),
	.user_rw(user_rw),
	.user_address(user_address),
	.user_data_in(user_data_in),
	.user_data_out(user_data_out),
	.user_ready(user_ready)
);

reg [31:0] op_counter_reg;
reg [31:0] op_counter_next;

reg user_start_next;
reg user_rw_next;
reg [15:0] user_address_next;
reg [15:0] user_data_in_next;


assign operations = op_counter_reg[7:0];

always@(posedge user_ready)
begin
	op_counter_reg <= op_counter_next;
	
	user_address <= user_address_next;
	user_data_in <= user_data_in_next;
	user_rw <= user_rw_next;
	user_start <= user_start_next;
	init_reg <= init_next;
end

wire [3:0] op_counter_minus_five = op_counter_reg - 5;

reg init_next;
reg init_reg = 1'b0;

always@*
begin
	user_data_in_next = 0;
	init_next = init_reg;
	op_counter_next = op_counter_reg;
	
	if(!init_reg) begin
		op_counter_next = op_counter_reg + 1;
		case(op_counter_next)
			1,2,3,4,5,6,7,8: begin
				user_start_next = 1;
				user_rw_next = 1; // write
				user_address_next = op_counter_reg[15:0];
				user_data_in_next = 1 << op_counter_reg;
			end
			default: begin
				init_next = 1'b1;
				user_start_next = 1;
				user_rw_next = 0; // read
				user_address_next = {8'b00000000, read_addr};
			end
		endcase
	end
	else begin
		user_start_next = 1;
		user_rw_next = 0; // read
		user_address_next = {8'b00000000, read_addr};
	end
end

wire [7:0] h2sseg_net0;
wire [7:0] h2sseg_net1;
wire [7:0] h2sseg_net2;
wire [7:0] h2sseg_net3;

hex_to_sseg h2sseg0 (
	.hex(user_data_out[3:0]),
	.dp(1'b1),
	.sseg(h2sseg_net0)
);
hex_to_sseg h2sseg1 (
	.hex(user_data_out[7:4]),
	.dp(1'b1),
	.sseg(h2sseg_net1)
);
hex_to_sseg h2sseg2 (
	.hex(user_data_out[11:8]),
	.dp(1'b1),
	.sseg(h2sseg_net2)
);
hex_to_sseg h2sseg3 (
	.hex(user_data_out[15:12]),
	.dp(1'b1),
	.sseg(h2sseg_net3)
);

disp_mux display (
	.clk(clk),
	.reset(1'b0),
	.in0(h2sseg_net0),
	.in1(h2sseg_net1),
	.in2(h2sseg_net2),
	.in3(h2sseg_net3),
	.an(an),
	.sseg(sseg)
);

endmodule
